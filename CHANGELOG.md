# 版本变更日志

这个项目的所有关键变化都将记录在此文件中.

此日志格式基于 [Keep a Changelog](https://keepachangelog.com/zh-CN/1.0.0/),
并且此项目遵循 [Semantic Versioning](https://semver.org/lang/zh-CN/).

## [Unreleased]

### Added

- 补充直接内容相关 api
- 应用功能新增类型: UIACTION(界面行为)

### Changed

- PSAppFuncImpl 类中：删除 getPSAppUIAction，新增 getPSUIAction

## [0.0.22] - 2022-03-06

### Added

- 补充(同步代理对象)加载测试

### Fixed

- ModelService 补充预加载模型

## [0.0.21] - 2022-02-11

### Fixed

- system 参数使用错误，导致未加载系统模型前使用

## [0.0.20] - 2022-02-10

### Added

- 补充模型

### Change

- 模型 getParentPSModelObject 逻辑中，app.IPSApplication、dataentity.IPSDataEntity、app.dataentity.IPSAppDataEntity、dataentity.service.IPSDEServiceAPI 特殊处理