import { IPSControl } from '@ibizlab/model';
import { Locator } from '@playwright/test';

/**
 * 部件
 *
 * @author chitanda
 * @date 2022-04-17 16:04:15
 * @export
 * @class IBizControl
 */
export class IBizControl {
  /**
   * Creates an instance of IBizControl.
   *
   * @author chitanda
   * @date 2022-04-21 17:04:08
   * @param {Page} page 页面
   * @param {IPSControl} [model]
   */
  constructor(public locator: Locator, public model: IPSControl) {}
}
