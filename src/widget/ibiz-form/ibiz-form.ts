import { IBizControl } from '../ibiz-control/ibiz-control';

/**
 * 表单
 *
 * @author chitanda
 * @date 2022-04-17 16:04:45
 * @export
 * @class IBizForm
 * @extends {IBizControl}
 */
export class IBizForm extends IBizControl {
  /**
   * 获取表单项值
   *
   * @author chitanda
   * @date 2022-04-17 16:04:21
   * @param {string} codeName
   * @return {*}  {Promise<string>}
   */
  async getValue(codeName: string): Promise<string> {
    const formItem = this.locator.locator(`.app-form-item2.${codeName}`);
    const val = await formItem.locator('input').inputValue();
    return val;
  }

  /**
   * 填充表单值
   *
   * @author chitanda
   * @date 2022-04-20 19:04:08
   * @param {string} codeName
   * @param {string} value
   * @return {*}  {Promise<void>}
   */
  async setValue(codeName: string, value: string): Promise<void> {
    const formItem = this.locator.locator(`.app-form-item2.${codeName}`);
    await formItem.locator('input').fill(value);
  }

  /**
   * 填充表单
   *
   * @author chitanda
   * @date 2022-04-21 20:04:05
   * @param {Record<string, string>} data
   */
  async fill(data: Record<string, string>) {
    for (const key in data) {
      const val = data[key];
      await this.setValue(key, val);
    }
  }
}
