import { IBizControl } from '../ibiz-control/ibiz-control';

/**
 * 表格
 *
 * @author chitanda
 * @date 2022-04-21 17:04:10
 * @export
 * @class IBizGrid
 * @extends {IBizControl}
 */
export class IBizGrid extends IBizControl {}
