import { IBizControl } from '../ibiz-control/ibiz-control';

/**
 * 工具栏
 *
 * @author chitanda
 * @date 2022-04-21 16:04:05
 * @export
 * @class IBizToolbar
 * @extends {IBizControl}
 */
export class IBizToolbar extends IBizControl {
  /**
   * 工具栏项点击
   *
   * @author chitanda
   * @date 2022-04-21 20:04:55
   * @param {string} tag 工具栏项标识
   * @return {*}  {Promise<void>}
   */
  click(tag: string): Promise<void> {
    return this.locator.locator(`.ivu-btn.${tag}`).click();
  }
}
