import { IBizViewSelector } from './ibiz-view-selector/ibiz-view-selector';

/**
 * 视图常量与选择器
 *
 * @author chitanda
 * @date 2022-04-22 10:04:29
 * @export
 * @class IBizViewConst
 */
export class IBizViewConst {
  /**
   * 视图相关选择器
   *
   * @author chitanda
   * @date 2022-04-22 10:04:45
   * @static
   */
  static selector = new IBizViewSelector();
}
