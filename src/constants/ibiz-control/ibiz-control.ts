import { IBizControlSelector } from './ibiz-control-selector/ibiz-control-selector';

/**
 * 部件常量与选择器
 *
 * @author chitanda
 * @date 2022-04-22 10:04:21
 * @export
 * @class IBizControlConst
 */
export class IBizControlConst {
  /**
   * 部件相关选择器
   *
   * @author chitanda
   * @date 2022-04-22 10:04:54
   * @static
   */
  static selector = new IBizControlSelector();
}
