export class IBizControlSelector {
  /**
   * 表格选择器
   *
   * @author chitanda
   * @date 2022-04-22 10:04:51
   * @readonly
   * @type {string}
   */
  get grid(): string {
    return this.getControl('grid');
  }

  /**
   * 表单选择器
   *
   * @author chitanda
   * @date 2022-04-22 10:04:49
   * @readonly
   * @type {string}
   */
  get form(): string {
    return this.getControl('form');
  }

  /**
   * 获取部件选择器
   *
   * @author chitanda
   * @date 2022-04-22 10:04:05
   * @param {string} tag
   * @return {*}  {string}
   */
  getControl(tag: string): string {
    return `.control-container.${tag}`;
  }
}
