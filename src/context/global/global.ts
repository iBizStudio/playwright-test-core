import { BrowserContext } from '@playwright/test';

/**
 * 全局上下文扩展处理类
 *
 * @author chitanda
 * @date 2022-04-21 19:04:19
 * @export
 * @class GlobalContext
 */
export class GlobalContext {
  /**
   * Creates an instance of GlobalContext.
   *
   * @author chitanda
   * @date 2022-04-21 19:04:53
   * @param {BrowserContext} ctx
   */
  constructor(public ctx: BrowserContext) {}

  /**
   * 设置认证 token
   *
   * @author chitanda
   * @date 2022-04-21 19:04:41
   * @param {string} token
   */
  addToken(token: string): void {
    this.ctx.addCookies([{ name: 'ibzuaa-token', value: token, domain: 'l.zhr.icu', path: '/' }]);
  }
}
