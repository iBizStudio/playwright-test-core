import { GlobalContext } from '../global/global';

/**
 * 视图上下文扩展处理类
 *
 * @author chitanda
 * @date 2022-04-21 19:04:17
 * @export
 * @class ViewContext
 */
export class ViewContext extends GlobalContext {}
