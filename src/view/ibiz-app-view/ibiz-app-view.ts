import { IBizView } from '../ibiz-view/ibiz-view';

/**
 * 应用视图基类
 *
 * @author chitanda
 * @date 2022-04-22 09:04:40
 * @export
 * @class IBizAppView
 * @extends {IBizView}
 */
export abstract class IBizAppView extends IBizView {
  protected async onInit(): Promise<void> {
    this.l = await this.el('#' + this.viewModel.codeName);
  }

  /**
   * 部件初始化
   *
   * @author chitanda
   * @date 2022-04-22 09:04:44
   * @protected
   * @abstract
   * @return {*}  {Promise<void>}
   */
  protected abstract onControlInit(): Promise<void>;
}
