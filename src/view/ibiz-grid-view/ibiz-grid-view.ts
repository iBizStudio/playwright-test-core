import { getCtrl, IPSDEGrid } from '@ibizlab/model';
import { IBizControlConst } from '../../constants';
import { IBizGrid } from '../../widget';
import { IBizAppView } from '../ibiz-app-view/ibiz-app-view';

/**
 * 表格视图
 *
 * @author chitanda
 * @date 2022-04-20 16:04:33
 * @export
 * @class IBizGridView
 * @extends {IBizAppView}
 */
export class IBizGridView extends IBizAppView {
  /**
   * 表格部件
   *
   * @author chitanda
   * @date 2022-04-22 09:04:40
   * @type {IBizGrid}
   */
  grid!: IBizGrid;

  protected async onControlInit(): Promise<void> {
    const control = await this.el(IBizControlConst.selector.grid);
    const model = getCtrl(this.viewModel, 'grid') as IPSDEGrid;
    this.grid = new IBizGrid(control, model);
  }
}
