import { getCtrl, IPSDEEditForm } from '@ibizlab/model';
import { IBizControlConst } from '../../constants';
import { IBizForm } from '../../widget';
import { IBizAppView } from '../ibiz-app-view/ibiz-app-view';

/**
 * 编辑视图
 *
 * @author chitanda
 * @date 2022-04-21 20:04:46
 * @export
 * @class IBizEditView
 * @extends {IBizAppView}
 */
export class IBizEditView extends IBizAppView {
  /**
   * 编辑表单
   *
   * @author chitanda
   * @date 2022-04-21 20:04:59
   * @type {IPSDEEditForm}
   */
  form!: IBizForm;

  protected async onControlInit(): Promise<void> {
    const control = await this.el(IBizControlConst.selector.form);
    const model = getCtrl(this.viewModel, 'form') as IPSDEEditForm;
    this.form = new IBizForm(control, model);
  }
}
