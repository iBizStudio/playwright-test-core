/**
 * 系统相关配置
 *
 * @author chitanda
 * @date 2022-04-19 18:04:22
 * @export
 * @interface ISystemEnvironment
 */
export interface ISystemEnvironment {
  /**
   * 系统标识
   *
   * @author chitanda
   * @date 2022-04-19 18:04:15
   * @type {string}
   */
  id: string;
}
