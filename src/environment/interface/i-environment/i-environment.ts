import { ISystemEnvironment } from '../i-system-environment/i-system-environment';
import { IUserEnvironment } from '../i-user-environment/i-user-environment';

/**
 * 配置参数
 *
 * @author chitanda
 * @date 2022-04-17 11:04:48
 * @export
 * @interface IEnvironment
 */
export interface IEnvironment {
  /**
   * 系统相关配置
   *
   * @author chitanda
   * @date 2022-04-19 18:04:00
   * @type {ISystemEnvironment}
   */
  system: ISystemEnvironment;
  /**
   * 用户相关配置
   *
   * @author chitanda
   * @date 2022-04-19 18:04:44
   * @type {IUserEnvironment}
   */
  user: IUserEnvironment;
}
