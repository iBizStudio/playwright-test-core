/**
 * 用户相关配置
 *
 * @author chitanda
 * @date 2022-04-19 18:04:25
 * @export
 * @interface IUserEnvironment
 */
export interface IUserEnvironment {
  /**
   * 用户认证 token
   *
   * @author chitanda
   * @date 2022-04-21 19:04:04
   * @type {string}
   */
  token?: string;
  /**
   * 用户名
   *
   * @author chitanda
   * @date 2022-04-17 11:04:28
   * @type {string}
   */
  username: string;
  /**
   * 密码
   *
   * @author chitanda
   * @date 2022-04-17 11:04:22
   * @type {string}
   */
  password: string;
}
