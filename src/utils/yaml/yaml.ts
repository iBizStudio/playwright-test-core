import * as yaml from 'js-yaml';
import { readFileSync } from 'fs-extra';
import { normalize } from 'path';

/**
 * 读取 yaml 文件
 *
 * @author chitanda
 * @date 2022-04-19 09:04:16
 * @export
 * @param {string} path
 * @return {*}  {unknown}
 */
export function readYaml(path: string): unknown {
  return yaml.load(readFileSync(normalize(path), 'utf-8'));
}
