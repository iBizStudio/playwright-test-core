import { getApp, IPSApplication, PSModelServiceImpl } from '@ibizlab/model';
import * as fs from 'fs-extra';
import * as path from 'path';

/**
 * 模型服务
 *
 * @author chitanda
 * @date 2022-04-20 16:04:22
 * @export
 * @class ModelService
 */
export class ModelService extends PSModelServiceImpl {
  /**
   * 当前配置的应用
   *
   * @author chitanda
   * @date 2022-04-21 16:04:28
   * @type {IPSApplication}
   */
  readonly app: IPSApplication;

  /**
   * Creates an instance of ModelService.
   *
   * @author chitanda
   * @date 2022-04-22 12:04:11
   * @param {string} sysModelPath 系统模型根路径，例：D:/workspace/ibizstudioplugin/model
   * @param {string} appModelPath 当前测试应用模型标识，例：PSSYSAPPS/LogicDesign/PSSYSAPP.json
   */
  constructor(sysModelPath: string, appModelPath: string) {
    super((modelPath: string): IModel => {
      const pathStr = path.normalize(path.join(path.normalize(sysModelPath), path.normalize(modelPath)));
      const modelStr = fs.readFileSync(pathStr, 'utf8');
      return JSON.parse(modelStr);
    });
    this.app = getApp(this.sys, appModelPath) as IPSApplication;
  }
}
